import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

from .models import Presentation
from events.models import Conference
from common.json import MainEncoder


############################


class PresentationListEncoder(MainEncoder):
    model = Presentation
    properties = ["title"]

    def get_extra_data(self, o):
        return {"status": o.status.name}


############################


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.

    {
        "presentations": [
            {
                "title": presentation's title,
                "status": presentation's status name
                "href": URL to the presentation,
            },
            ...
        ]
    }
    """

    # # # OLD WAY:
    # presentations = [
    #     {
    #         "title": p.title,
    #         "status": p.status.name,
    #         "href": p.get_api_url(),
    #     }
    #     for p in Presentation.objects.filter(conference=conference_id)
    # ]

    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
        )

    elif request.method == "POST":
        new_content = json.loads(request.body)

        try:
            conference = Conference.objects.get(id=conference_id)
            new_content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "invalid conference id"},
                status=400,
            )

        new_presentation = Presentation.create(**new_content)

        return JsonResponse(
            new_presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


# ========================================================================


#########################


class PresentationDetailEncoder(MainEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]


#########################


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_presentation(request, id):
    """
    Returns the details for the Presentation model specified
    by the id parameter.

    This should return a dictionary with the presenter's name,
    their company name, the presenter's email, the title of
    the presentation, the synopsis of the presentation, when
    the presentation record was created, its status name, and
    a dictionary that has the conference name and its URL

    {
        "presenter_name": the name of the presenter,
        "company_name": the name of the presenter's company,
        "presenter_email": the email address of the presenter,
        "title": the title of the presentation,
        "synopsis": the synopsis for the presentation,
        "created": the date/time when the record was created,
        "status": the name of the status for the presentation,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    if request.method == "GET":
        """

        Currently, the exception handling block will never be executed because you are trying to catch an exception on an object (presentation) that is already assumed to exist. Instead, you should try to get the Presentation object inside the try block and handle the DoesNotExist exception if it occurs.

        Take try and except off and only leave this below for "GET" if you want to keep it the original solution. Dan just did this try/except to avoid the yellow error page:

            presentation = Presentation.objects.get(id=id)
            return JsonResponse(
                presentation,
                encoder=PresentationDetailEncoder,
                safe=False,
            )

        """
        try:
            presentation = Presentation.objects.get(id=id)
            return JsonResponse(
                presentation,
                encoder=PresentationDetailEncoder,
                safe=False,
            )
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid presentation id"},
                status=400,
            )

    elif request.method == "DELETE":
        count, _ = Presentation.objects.get(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:  # if request.method = "PUT" (UPDATING SPECIFIC PRESENTATION)
        updated_content = json.loads(request.body)
        Presentation.objects.filter(id=id).update(**updated_content)
        updated_presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            updated_presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )

    # # OLD METHOD (filling each value in ourselves... slow):
    # return JsonResponse(
    #     {
    #         "presenter_name": presentation.presenter_name,
    #         "company_name": presentation.company_name,
    #         "presenter_email": presentation.presenter_email,
    #         "title": presentation.title,
    #         "synopsis": presentation.synopsis,
    #         "created": presentation.created,
    #         "status": presentation.status.name,
    #         "conference": {
    #             "name": presentation.conference.name,
    #             "href": presentation.conference.get_api_url(),
    #         },
    #     }
    # )

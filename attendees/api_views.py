import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

from .models import Attendee
from events.models import Conference
from common.json import MainEncoder
from events.api_views import ConferenceListEncoder


###################


class AttendeeListEncoder(MainEncoder):
    model = Attendee
    properties = ["name"]


###################


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """

    # # OLD WAY:
    # attendees = [
    #     {"name": attendee.name, "href": attendee.get_api_url()}
    #     for attendee in Attendee.objects.filter(conference=conference_id)
    # ]

    # READING THE LIST OF ATTENDEES FOR A SPECIFIC CONFERENCE:
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )

    # CREATING A NEW ATTENDEE FOR A SPECIFIC CONFERENCE:
    else:  # if request.method == "POST"
        content = json.loads(request.body)

        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message:" "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)

        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


# ========================================================================


###################


class AttendeeDetailEncoder(MainEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }


###################


@require_http_methods(["PUT", "GET", "DELETE"])
def api_show_attendee(request, id):
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """

    # attendee_detail = [
    #     {
    #         "email": attendee.email,
    #         "name": attendee.name,
    #         "company_name": attendee.company_name,
    #         "created": attendee.created,
    #         "conference": {
    #             "name": attendee.conference.name,
    #             "href": attendee.conference.get_api_url(),
    #         },
    #     }
    # ]

    # READING THE DETAILS OF A SPECIFIC ATTENDEE:
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )

    # DELETING A SPECIFIC ATTENDEE:
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    # UPDATING A SPECIFIC ATTENNDEE:   ##### DOUBLE CHECK THIS ONE ******
    elif request.method == "PUT":
        updated_content = json.loads(request.body)

        try:
            Attendee.objects.filter(id=id).update(**updated_content)
            updated_attendee = Attendee.objects.get(id=id)
            return JsonResponse(
                updated_attendee,
                encoder=AttendeeDetailEncoder,
                safe=False,
            )
        except Attendee.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid attendee id"},
                status=400,
            )

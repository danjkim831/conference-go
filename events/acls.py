from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

# Note: Any code in here should return Python dictionaries to be used by the view code:


# ACL method that uses the Pexel Search endpoint to search
# for an image related to the created location's city and state:
def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search"
    params = {
        "per_page": 1,
        "query": f"{city} {state}",
    }

    response = requests.get(url, params=params, headers=headers)
    content = response.json()
    #  ^ You can directly parse the response into a JSON format using .json()
    # Instead of json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, ValueError):
        return None


## =========================================================================================================


def get_weather_data(city, state):
    # TOP HALF IS FOR GEOCODING API -- FINDING LATITUDE/LONGITUDE FOR SPECIFIC CONFERENCE LOCATION:
    params = {
        "q": f"{city}, {state}, US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)  # or content = response.json()
    # content here returns a list with a dictionary inside containing info about response
    # print(content)
    # print(content[0])
    # print(content[0]["lat"])
    # print(content[0]["lon"])
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, ValueError):
        return None

    # BOTTOM HALF IS FOR CURRENT WEATHER API USING ABOVE LAT/LON COORDINATES:
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(
        response.content
    )  # or you can do: content = response.json()

    # # PRINT TO TEST:
    # print(content)
    # print(content["weather"][0]["description"])
    # print(content["main"]["temp"])

    try:
        return {
            "temp": f'{content["main"]["temp"]} farenheit',
            "description": content["weather"][0]["description"],
        }
    except (KeyError, ValueError):
        return None

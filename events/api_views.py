import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

from .models import Conference, Location, State
from events.models import Location
from events.acls import get_photo, get_weather_data
from common.json import MainEncoder


############################


class ConferenceListEncoder(MainEncoder):
    model = Conference
    properties = ["name"]


############################


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    # response = []
    # conferences = Conference.objects.all()
    # for conference in conferences:
    #     response.append(
    #         {
    #             "name": conference.name,
    #             "href": conference.get_api_url(),
    #         }
    #     )

    # SHOWING/READING A FULL LIST OF CONFERENCES:
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )

    # CREATING A NEW CONFERENCE:
    else:
        # request.body is a JSON formatted string converted to a dictionary named content (using loads)
        new_content = json.loads(request.body)

        # Get the Location object and put it into the content dict
        try:
            location = Location.objects.get(id=new_content["location"])
            new_content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        new_conference = Conference.objects.create(**new_content)
        return JsonResponse(
            new_conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

    # # # OLD WAY (filling each value in ourselves... slow):
    # conferences = [
    #     {"name": conference.name, "href": conference.get_api_url()}
    #     for conference in Conference.objects.all()
    # ]

    # return JsonResponse({"conferences": conferences})


# ========================================================================


###############


class LocationListEncoder(MainEncoder):
    model = Location
    properties = ["name"]


###############


class ConferenceDetailEncoder(MainEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


###############


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_conference(request, id):
    """
    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """

    if request.method == "GET":
        conference = Conference.objects.get(id=id)

        # Use the city and state abbreviation of the Conference's Location
        # to call the get_weather_data ACL function and get back a dictionary
        # that contains the weather data
        weather = get_weather_data(
            conference.location.city,
            conference.location.state.abbreviation,
        )
        return JsonResponse(
            {"conference": conference, "weather": weather},
            encoder=ConferenceDetailEncoder,
        )

    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        updated_content = json.loads(request.body)

        try:
            location = Location.objects.get(id=updated_content["location"])
            updated_content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        Conference.objects.filter(id=id).update(**updated_content)
        updated_conference = Conference.objects.get(id=id)

        return JsonResponse(
            updated_conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

    """
     Quick Overview of what's happening: 
    1. the conference object is passed to JsonResponse
    2. JsonResponse calls json.dumps, using ConferenceDetailEncoder for custom serialization
    3. the default method of ConferenceDetailEncoder handles the custom serialization of the conference object and its properties
    4. if a datetime object is encountered, DateEncoder converts it intto an ISO8601 string
    5. the result (JSON string) is returned in the HTTP response

    """

    # ========================================================================


# This decorator only allows GET and POST requests to this method:
@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """

    # response = []
    # locations = Location.objects.all()
    # for location in locations:
    #     response.append(
    #         {"name": location.name, "href": location.get_api_url()}
    #     )

    # locations = [
    #     {"name": location.name, "href": location.get_api_url()}
    #     for location in Location.objects.all()
    # ]

    # SHOWING/READING A FULL LIST OF LOCATIONS:
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )

    # CREATE A NEW LOCATION:
    else:  # if request.method == "POST"
        # # the content of the POST is a JSON-formatted string stored in "request.body"
        new_content = json.loads(request.body)

        # get the State object and put it the content dict
        try:
            state = State.objects.get(abbreviation=new_content["state"])
            new_content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        photo = get_photo(new_content["city"], state)
        new_content.update(photo)

        new_location = Location.objects.create(**new_content)

        return JsonResponse(
            new_location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


# ========================================================================


###############


class LocationDetailEncoder(MainEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


###############


# Protect this method/view to respond only to GET, PUT, and DELETE HTTP methods:
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """

    # SHOWING/READING A SPECIFIC LOCATION:
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

    # DELETING A SPECIFIC LOCATION:
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    # UPDATING A SPECIFIC LOCATION:
    else:  # if request.method == "PUT"
        updated_content = json.loads(request.body)
        try:
            if "state" in updated_content:
                state = State.objects.get(
                    abbreviation=updated_content["state"]
                )
                updated_content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        # The reason why you don't assign a variable to this is bc you're just updating the Location object and unpacking the content dict into it
        Location.objects.filter(id=id).update(**updated_content)

        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

    # # # OLD WAY (filling each value in ourselves... slow):
    # return JsonResponse(
    #     {
    #         "name": location.name,
    #         "city": location.city,
    #         "room_count": location.room_count,
    #         "created": location.created,
    #         "updated": location.updated,
    #         "state": location.state.abbreviation,
    #     }
    # )

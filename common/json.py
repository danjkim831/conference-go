from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


# # Encodes QuerySets (fancy list(like) tied to the data in the database):
# # This encoder will convert a QuerySet to a list
# class QuerySetEncoder(JSONEncoder):
#     def default(self, o):
#         if isinstance(o, QuerySet):
#             return list(o)
#         else:
#             return super().default(o)


# # ========================================================================


# # Encodes date format:
# # We want our ModelEncoder to have the functionality of DateEncoder,
# # so we add DateEncoder to ModelEncoder inheritance list before the JSONEncoder
# # Also, DateEncoder needs to be above ModelEncoder
# # (or else DateEncoder will be undefined when ModelEncoder attemps to inherit!)
# class DateEncoder(JSONEncoder):
#     def default(self, o):
#         # if o is an instance of datetime
#         #    return o.isoformat()
#         # otherwise
#         #    return super().default(o)

#         if isinstance(o, datetime):
#             return o.isoformat()
#         else:
#             return super().default(o)


# ========================================================================


# # Encodes simple formats like strings,nums,etc:
# class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):

#     #   if the object to decode is the same class as what's in the
#     #   model property, then
#     #     * create an empty dictionary that will hold the property names
#     #       as keys and the property values as values
#     #     * for each name in the properties list
#     #         * get the value of that property from the model instance
#     #           given just the property name
#     #         * put it into the dictionary with that property name as
#     #           the key
#     #     * return the dictionary
#     #   otherwise,
#     #       return super().default(o)  # From the documentation

#     # when a subclass of ModelEncoder is created (ex: ConferenceDetailEncoder or LocationListEncoder), it can override this encoder to specify custom encoders for specific properties. If a subclass didnt provide this attribute, this encoders woud default to an empty dictionary
#     # this empty encoders dict is basically a safeguard and default value that ensures the ModelEncoder class and its subclasses function corrector, even if custom encoders arent specified
#     encoders = {}


# def default(self, o):
#     if isinstance(o, self.model):
#         d = {}

#         # THIS IS PRIMARILY FOR LIST VIEWS (they have hrefs!):
#         # if o has the attribute get_api_url,
#         #   then add its return value to the dictionary
#         #   with the key "href"
#         # REMEMBER: this is HASATTR to check if object HAS attribute
#         if hasattr(o, "get_api_url"):
#             d["href"] = o.get_api_url()

#         for property in self.properties:
#             # o is the object (ex: specific Conference instance object)
#             # property is the attribute of that instance (ex: "name")
#             value = getattr(o, property)

#             # explanation below
#             if property in self.encoders:
#                 encoder = self.encoders[property]
#                 value = encoder.default(value)

#             d[property] = value

#         # if there's extra data in a subclass, we can
#         # update the dict with it here
#         d.update(self.get_extra_data(o))

#         return d

#     else:
#         return super().default(o)


# def get_extra_data(self, o):
#     return {}


class MainEncoder(JSONEncoder):
    encoders = {}

    def default(self, o):

        if isinstance(o, self.model):
            d = {}

            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()

                for property in self.properties:
                    value = getattr(o, property)

                    if property in self.encoders:
                        encoder = self.encoders[property]
                        value = encoder.default(value)

                    d[property] = value

                d.update(self.get_extra_data(o))

                return d

        elif isinstance(o, QuerySet):
            return list(o)

        elif isinstance(o, datetime):
            return o.isoformat()

        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}
